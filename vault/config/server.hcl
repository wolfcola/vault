listener "tcp" {
  address                         = "0.0.0.0:8200"
  cluster_address                 = "0.0.0.0:8201"
  proxy_protocol_behavior         =  "use_always"
//proxy_protocol_authorized_addrs = [""]
  tls_cert_file                   = "/certs/vault.pem"
  tls_key_file                    = "/certs/vault.key.pem"
}

// service_registration "consul" {
//   address      = "infra.wolfcola.me"
//   scheme       = "http"
// //  token        = "VAULT_TOKEN_FROM_CONSUL"
// // tls_ca_file   = "/etc/pem/vault.ca"
// // tls_cert_file = "/etc/pem/vault.cert"
// // tls_key_file  = "/etc/pem/vault.key"
// }

storage "raft" {
  path = "/vault/file"
  node_id = "vault-server"
}
api_addr      = "https://vault.wolfcola.me"
cluster_addr  = "https://clstr.vault.wolfcola.me"
cluster_name  = "vault"
disable_mlock = true
ui            = true
log_level     = "info"
log_format    = "json"
log_file      = "/vault/log"


