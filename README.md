# Create certs usinf consul cli
create ca
> consul tls ca create -domain=vault -common-name=Vault 

create server certs
> consul tls cert create -server -ca=vault-agent-ca.pem -domain=vault -key=vault-agent-ca-key.pem 

copy certs into certs folder
> cp dc1-server-vault-0.pem certs/vault.pem  
> cp dc1-server-vault-0-key.pem certs/vault.key.pem 

<br>

# Vault in Docker Standalone
This repository contains the playbook to deploy a vault server and configure it with some certifactes. 

The config is in config/server.hcl, this should only have the initial config to bootstrap the server, all other config should be done through terraform. 

you can demo the container before you deploy it with the docker compose file

run 
> docker-compose up

then in your terminal
> export VAULT_ADDR="https://localhost:8200"

To get the vault token navigate to the vaults [UI](https://localhost:8200)
This will tell you to unseal the vault and give you a root token to log in with
> export VAULT_TOKEN=""


in the UI enable the kv secret engine and keep the name kv and add a secret with path test and just throw some dummy values in

then in your cli 
> vault kv get -mount=kv -tls-skip-verify -format=json test 




# Ansible Deploy
to deploy go into the root folder, add the correct ip of the host to deploy it too

> ansible-playbook -i hosts playbooks/playbook.yml --ask-vault-pass


# create a secret 
create a secret on kv engine with path credentials and a field password for the test to work

> kv/credentials -field=password

> export VAULT_TOKEN="$(vault write -tls-skip-verify -field=token auth/jwt/login role=test-role jwt=$CI_JOB_JWT)"

**Must be done on runner that can reach private vault**