terraform {
  required_providers {
    vault = {
      source  = "hashicorp/vault"
      version = "3.14.0"
    }
  }
  backend "http" {
    
  }
}
variable "VAULT_TOKEN" {
    type = string
    sensitive = true
}

provider "vault" {
  address         = "https://vault.matrix.wolfcola.me" # Configuration options
  token           = var.VAULT_TOKEN
  skip_tls_verify = true
}