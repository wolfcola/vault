resource "vault_policy" "wolfcola_gitlab" {
  name = "wolfcola-gitlab"

  policy = file("./policies/wolfcola-gitlab.hcl")
}

resource "vault_policy" "admin" {
  name = "admin"

  policy = file("./policies/admin.hcl")
}

resource "vault_policy" "wc_admin" {
  name = "wolfcola-admin"

  policy = file("./policies/wolfcola-admin.hcl")
}

resource "vault_jwt_auth_backend_role" "wolfcola_matrix_vault" {
  backend         = vault_jwt_auth_backend.gitlab_jwt.path
  role_name       = "45017487"
  token_policies  = ["default", vault_policy.wolfcola_gitlab.name, vault_policy.admin.name, vault_policy.wc_admin.name ]
  token_explicit_max_ttl = 3600
  bound_claims_type = "glob"
  bound_claims = {
    project_id = "45017487"
  }
  user_claim      = "user_email"
  role_type       = "jwt"
}

resource "vault_mount" "wolfcola" {
  path        = "wolfcola"
  type        = "kv"
  options     = { version = "2" }
  description = "KV Version 2 secret engine mount"
  
}

resource "vault_kv_secret_backend_v2" "wolfcola" {
  mount                = vault_mount.wolfcola.path
#   max_versions         = 5
#   delete_version_after = 12600
#   cas_required         = true
}
