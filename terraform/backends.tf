resource "vault_jwt_auth_backend" "gitlab_jwt" {
    description         = "Gitlab JWT auth backend"
    path                = "jwt"
    jwks_url  = "https://gitlab.com/-/jwks"
    bound_issuer        = "gitlab.com"
}
